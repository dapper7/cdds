#include "Example1App.h"
#include "Texture.h"
#include "Font.h"
#include "Input.h"
#include <iostream>
#include <sstream>
#include "Clicker.h"
#include "Score.h"


using namespace aie;

unsigned int Example1App::getAddToScore()
{
//	m_score.addToScore;
	return 0;
}
Example1App::Example1App() 
{

}

Example1App::~Example1App() 
{

}

bool Example1App::startup() {
	
	m_2dRenderer = new aie::Renderer2D();

	// TODO: remember to change this when redistributing a build!
	// the following path would be used instead: "./font/consolas.ttf"
	m_font = new aie::Font("../bin/font/consolas.ttf", 32);
	
	m_animation.load("../bin/textures/cheese/cheese", 22, 1, true, true);
	
	m_scoreBoard = new aie::Texture("../bin/textures/scoreBoard.png");
	return true;
}

void Example1App::shutdown() {

	delete m_font;
	delete m_2dRenderer;
	//delete m_clicker;
	delete m_scoreBoard;
}

void Example1App::update(float deltaTime) {

	// input example
	aie::Input* input = aie::Input::getInstance();

	m_animation.update(deltaTime);

	// If user clicks left mouse button, increase score by 1
	if (input->wasMouseButtonPressed(aie::INPUT_MOUSE_BUTTON_LEFT))
	{
		m_score.addToScore(1);
		/*for (int i; m_score.getScore < 10; i++)
		{
			m_clicker = new aie::Texture("../bin/textures/cheese1.png");
		}*/
	}

	// exit the application
	if (input->isKeyDown(aie::INPUT_KEY_ESCAPE))
		quit();
}
//hitbox
bool isPointInCircle(float xa, float ya, float xc, float yc, float r)
{
	return ((xa - xc)*(xa - xc) + (ya - yc)*(ya - yc)) < r*r;
}
void Example1App::draw() {

	// wipe the screen to the background colour
	clearScreen();

	// begin drawing sprites
	m_2dRenderer->begin();

	// draw your stuff here!
	//if ()
	m_2dRenderer->drawSprite(m_scoreBoard, 550, 650, 0, 0, 0, 0, 0, 0);
	//m_2dRenderer->drawSprite(m_clicker, 360, 100, 0, 0, 0, 0, 0, 0);
	
	m_animation.draw(m_2dRenderer, 640, 360);

	// output some text, uses the last used colour
	std::stringstream scoreString;
	scoreString << "Score: " << m_score.getScore();

	m_2dRenderer->drawText(m_font, scoreString.str().c_str(), 599, 663, 0);

	// done drawing sprites
	m_2dRenderer->end();
}

