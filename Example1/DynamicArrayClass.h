#pragma once
#include <algorithm>

using namespace std;

const unsigned int DefaultCapacity = 10;

template <typename T>
class DynamicArrayClass
{
private:
	unsigned int numOfEl;
	unsigned int capacity;
	T *arr;

	void expand()
	{
		// Double the capacity
		// Creating a temporary array with a capacity times two
		T *tempArr = new T[capacity*2];

		//using a for loop to place the numbers of elements into the temporary array
		for (size_t i = 0; i < numOfEl; i++)
		{
			tempArr[i] = arr[i];
		}
		// deleting the old array			
		delete[] arr;
		// replacing temporary array to the main array
		arr = tempArr;
	}
	
	

	// a function to delete the current array
	void freeArrayData()
	{
		// if arr does not = nullptr
		if (arr != nullptr)
		{
			//delete the array
			delete[] arr;
			//make arrays value equal to nullptr
			arr = nullptr;
		}
	}

	void allocatedData(unsigned int newAllocated)
	{
		//make the current array equal to nullptr
		freeArrayData();
		//make capacity equal to newAllocated
		capacity = newAllocated;
		//array is equal to a new capacity with the T template
		arr = new T[capacity];
		//making number of elements in the array equal to 0
		numOfEl = 0;
	}
public:
	
	//constructor
	//for this function were making allocatedCapacity equal to our constant value for the Capacity of the array
	DynamicArrayClass(unsigned int allocatedCapacity = DefaultCapacity)
	{
		//making arrays value is nullptr
		arr = nullptr;
		allocatedData(allocatedCapacity);
	}
	//deconstructor
	~DynamicArrayClass()
	{
		//deleting the array
		delete[] arr;
	}

	//to get the number of elements 
	unsigned int getNumOfEl()
	{
		return numOfEl;
	}

	//to get the capacity
	unsigned int getCapacity()
	{
		return capacity;
	}

	//to get the array
	T& getAt(unsigned int index)
	{
		//if the index (would be i for the forloop in main) is greater than or equal to the number of elements
		if (index >= numOfEl)
		{
			//output this message
			throw ("This is an ovut of bounds exception");
		}
		//otherwise return whats in the array
		return arr[index];
	}
	
	//push back elements onto the array
	void pushBack(const T& el)
	{
		//if number of elements is great to or equal to capacity
		if (numOfEl >= capacity)
		{
			//use the expand function
			expand();
		}

		//el is equal to the number pushing onto the array so number of elements will be increased +1
		arr[numOfEl++] = el;
	}
	

	//poping off an elements in the array
	T& popBack(const T& el)
	{
		//if the number of elements in the array is greater than 0
		if (numOfEl > 0)
		{
			//take 1 element off the array
			--numOfEl;
			//return the array with the number of elemtns
			return arr[numOfEl] = el;
		}
		//otherwise
		else
		{
			//output the message
			throw("Array empty");
		}
		
	}
};

