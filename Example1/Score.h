#pragma once

class Score
{
private:
	unsigned int score = 0;
public:
	Score();
	~Score();

	//returns the value of score
	unsigned int getScore();
	//set score
	void setScore(int value);

	//adds to score
	void addToScore(int value = 1);
	
};

