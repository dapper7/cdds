#include "Example1App.h"
#include <iostream>
#include "Score.h"
#include "scoreClicker.h"
#include "Example1App.h"
#include "Clicker.h"

int main() 
{
	

	// allocation
	auto app = new Example1App();

	// initialise and loop
	app->run("AIE", 1280, 720, false);

	// deallocation
	delete app;

	return 0;
}