// Author: Garreth Cheffirs
// 2014 - Introduction to C++ Assignment 1
// Modified 2017 - Sam Cartwright
// Academy of Interactive Entertainment
// This code is provided by AIE for the purposes of completing in-class tutorials
// Excessive use of this code in assessments, or its use without the appropriate 
// attribution will be treated as plagiarism.

#ifndef _ANIMATION_H_
#define _ANIMATION_H_

#include <glm/vec2.hpp>
#include "DynamicArrayClass.h"

namespace aie {
	class Renderer2D;
	class Texture;
};

class Animation
{
public:
	Animation();
	~Animation();
	//load function
	unsigned int load(const char* filename, unsigned int frames, float interval, bool loop = true, bool playing = true);
	//play function
	void play();
	//stop function
	void stop();
	//pause function
	void pause();
	//to ask if the animation is playing
	bool isPlaying();
	//set how fast the animation is
	void setCurrentFrame(int frame) { m_current = frame; }
	//how the animation updates
	void update(float deltaTime);
	//drawing the animation
	void draw(aie::Renderer2D* renderer, float x, float y);

private:
	unsigned int m_current;
	float m_interval;
	float m_elapsed;
	bool m_loop;
	bool m_isPlaying;

	DynamicArrayClass<aie::Texture*> m_textures;
};

#endif // _ANIMATION_H_