// Author: Garreth Cheffirs
// 2014 - Introduction to C++ Assignment 1
// Modified 2017 - Sam Cartwright
// Academy of Interactive Entertainment
// This code is provided by AIE for the purposes of completing in-class tutorials
// Excessive use of this code in assessments, or its use without the appropriate 
// attribution will be treated as plagiarism.

#include "Animation.h"
#include <Renderer2D.h>
#include <Texture.h>
#include <sstream>

//settings for the animation
Animation::Animation()
	:m_textures(50)
{
	m_current = 0;
	m_interval = 0.0f;
	m_elapsed = 0.0f;
	m_loop = false;
	m_isPlaying = false;
}
//loading in the animation settings
unsigned int Animation::load(const char* filename, unsigned int frames, float interval, bool loop, bool playing)
{	
	m_current = 0;
	m_interval = interval;
	m_elapsed = 0.0f;
	m_loop = loop;
	m_isPlaying = playing;

	//the texture
	aie::Texture* texture;
	for (unsigned int i = 0; i < frames; i++)
	{
		//stringstream so that it can be out putted as a string onto the screen
		stringstream textureFilename;
		//when it looks for the texutre it uses the filename
		textureFilename << filename;
		//goes through each image as it loops through each image as the filename is the same but with a different number at the end of it in numerical order
		textureFilename << (i+1);
		//what file the images are
		textureFilename << ".png";
		texture = new aie::Texture(textureFilename.str().c_str());
		if (texture != nullptr)
		{
			m_textures.pushBack(texture);
		}
	}
	return m_textures.getNumOfEl();
}
//deconstruct the animation
Animation::~Animation()
{
	for (unsigned int i = 0; i < m_textures.getNumOfEl(); i++)
	{
		delete m_textures.getAt(i);
	}
}
//play makes m_isPlaying true
void Animation::play()
{
	m_isPlaying = true;
}
//stop makes m_isPlaying false and m_current to 0 which stops it all together
void Animation::stop()
{
	m_isPlaying = false;
	m_current = 0;
}
//play makes m_isPlaying false
void Animation::pause()
{
	m_isPlaying = false;
}
//returns whatever isPlaying is
bool Animation::isPlaying()
{
	return m_isPlaying;
}
//updating the animation
void Animation::update(float deltaTime)
{
	//if isPlaying is no ture return
	if (!m_isPlaying)
		return;

	m_elapsed += deltaTime;

	if (m_elapsed > m_interval)
	{
		m_elapsed -= m_interval;

		m_current++;

		if (m_current >= m_textures.getNumOfEl())
		{
			if (m_loop)
			{
				m_current = 0;
			}
			else
			{
				m_current = m_textures.getNumOfEl();
				m_isPlaying = false;
			}
		}
	}
}
//draw the animation
void Animation::draw(aie::Renderer2D* renderer, float x, float y)
{
	renderer->drawSprite(m_textures.getAt(m_current), x, y);
}