#pragma once
#include "Score.h"


class Clicker
{
private:
	//default starting click
	unsigned int defaultClick = 1;
	
public:
	//getting the mouse input
	bool getMouseInput();

	//mouseInput makes addToScore bool true
	void checkIfClickerIsBeingClicked();

	//add click
	//void typeOfClick();

	Score m_score;
	Clicker();
	~Clicker ();

};

