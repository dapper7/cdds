#pragma once
#include "Application.h"
#include "Renderer2D.h"
#include <Texture.h>
#include "Score.h"
#include "animation.h"
class Example1App : public aie::Application {
public:

	Example1App();
	virtual ~Example1App();
	
	//startup
	virtual bool startup();
	//shutdown
	virtual void shutdown();

	//update function like in animation
	virtual void update(float deltaTime);
	//drawing the spirts
	virtual void draw();

	//so we can use functions in the score.h
	Score m_score;
	//so we can use functions in the animation.h
	Animation m_animation;
	//getting the addToScore();
	unsigned int getAddToScore();

protected:
	aie::Texture*		m_scoreBoard;
	aie::Texture*		m_clicker;
	aie::Renderer2D*	m_2dRenderer;
	aie::Font*			m_font;
};